import { NextPageContext } from 'next';
import * as React from 'react';
import { connect } from 'react-redux';

import Layout from '../src/components/Layout';
import { ContactList } from '../src/components/ContactList';
import { Inbox } from '../src/components/Inbox';
import { Profile } from '../src/components/Profile';
import { Contacts } from '../src/seeder/Contacts';
import { ChatArea } from '../src/components/ChatArea';
import { addChatThunk } from '../src/redux/chat/slice';

interface IProps {
  gipData: string;
  sendChat: Function;
  Messages: Object;
}
interface IState {
  selectedProfile: any;
  contactsList: Object;
  activePage: number;
}
export class IndexPage extends React.Component<IProps, IState> {
  static async getInitialProps({}: NextPageContext) {
    return {
      gipData: 'Data from getInitialProps of IndexPage'
    };
  }
  constructor(props: any) {
    super(props);
    this.appendMessage = this.appendMessage.bind(this);
    this.selectProfile = this.selectProfile.bind(this);
    this.filterContacts = this.filterContacts.bind(this);
    this.setActivePage = this.setActivePage.bind(this);
    this.state = {
      selectedProfile: Contacts[0],
      contactsList: Contacts,
      activePage: 1
    };
  }
  setActivePage(number: number) {
    this.setState({
      activePage: number
    });
  }
  componentDidMount() {
    console.log(this.props.gipData);
  }

  appendMessage(message: Object) {
    this.props.sendChat(message);
  }

  selectProfile(id: number) {
    const searchedContact = Contacts.find((element) => {
      return element.id === id;
    });
    this.setState({
      selectedProfile: searchedContact
    });
    this.setActivePage(2);
  }
  filterContacts(search: string) {
    const filteredList = Contacts.filter((contact, index) => {
      return contact.name.toLowerCase().includes(search.toLowerCase());
    });
    this.setState({
      contactsList: filteredList
    });
  }
  render() {
    return (
      <Layout activePage={this.state.activePage}>
        <Inbox />
        <ContactList
          selectContact={this.selectProfile}
          Contacts={this.state.contactsList}
          searchContacts={this.filterContacts}
          selectedProfile={this.state.selectedProfile}
        />
        <ChatArea
          setActivePage={this.setActivePage}
          selectedProfile={this.state.selectedProfile}
          Messages={this.props.Messages}
          appendMessage={this.appendMessage}
        />
        <Profile
          setActivePage={this.setActivePage}
          selectedProfile={this.state.selectedProfile}
        />
      </Layout>
    );
  }
}

const mapStateToProps = (state: any) => {
  return { Messages: state.chat };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    sendChat: (message: any) => dispatch(addChatThunk(message))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(IndexPage);
