export const DEV = process.env.NODE_ENV !== 'production';

export const SITE_NAME = 'MSM';
export const SITE_TITLE = 'Multi Social Media Messaging';
export const SITE_DESCRIPTION = 'Prototype for MSM';
export const SITE_IMAGE = '/static/site-image.jpg';
export const SITE_AUTHOR = 'cSprance';
