import React,{useState,useEffect} from 'react'
import styled from 'styled-components'
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import AttachFileOutlinedIcon from '@material-ui/icons/AttachFileOutlined';
import SendIcon from '@material-ui/icons/Send';
import InputBase from '@material-ui/core/InputBase';
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import StarIcon from '@material-ui/icons/Star';
import CallIcon from '@material-ui/icons/Call';
import VideocamIcon from '@material-ui/icons/Videocam';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
const outerTheme = createMuiTheme({
  palette: {
    type:'light'
  }
});

const useStyles = makeStyles((theme) => ({
  paperform: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  icons:{
    margin:'auto',
    color:'#838488'
  },
      text:{
        color:'#000'
    },
}));
const AvatarHolder = styled.div`
display:inline-block;
margin:12px;
`;
const ChatAreaWrapper = styled.div`
background:#f0f5f8;
display:grid;
min-height:100%;
grid-template-areas:
    "topbar"
    "chat"
    "input";
grid-template-rows: 5rem 1fr auto;
`;
const ChatHeader = styled.div`
grid-area: topbar;
display:grid;
grid-template-columns: 1fr 64px 64px 64px;
border-bottom: 1px solid #ccc;
@media only screen and (max-width: 768px) {
  grid-template-columns: 64px 1fr 64px 64px 64px;
}
`;
const ChatBody = styled.div`
grid-area: chat;

`;
const ChatInput = styled.div`
grid-area: input;
`;
const IconHolder = styled.div`
display:flex;
border-left: 1px solid #ccc;
`;
const SentMessageHolder = styled.div`
display:flex;
  margin-left: auto;
  max-width: 100%;
 
`;
const Sent =styled.div`
padding:16px;
display:flex;
align-items:flex-end;
`;
const Recieved = styled.div`padding:16px;`;
const RecievedMessageHolder= styled.div`
display:flex;
align-items:center;
`;
const ChatBGRecieved= styled.span`
background:#0086fe;
display:inline-block;
padding:1.2rem;
border-radius:8px;
position:relative;
  &:after,:before{	right: 100%;
	top: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;}
  &:after{border-color: rgba(136, 183, 213, 0);
	border-right-color: #0086fe;
	border-width: 10px;
	margin-top: -10px;}
  &:before{border-color: rgba(194, 225, 245, 0);
	border-right-color: #0086fe;
	border-width: 10px;
	margin-top: -10px;}
`;
const ChatBGSent= styled.span`
background:#fff;
color:#000;
display:inline-block;
padding:1.2rem;
border-radius:8px;
 position:relative;
  &:after,:before{	left: 100%;
	top: 50%;
	border: solid transparent;
	content: " ";
	height: 0;
	width: 0;
	position: absolute;
	pointer-events: none;}
  &:after{border-color: rgba(136, 183, 213, 0);
	border-left-color: #fff;
	border-width: 10px;
	margin-top: -10px;}
  &:before{border-color: rgba(194, 225, 245, 0);
	border-left-color: #fff;
	border-width: 10px;
	margin-top: -10px;}

`;
const NameHolder = styled.span`
font-size: 1rem;
font-weight: 500;
margin: auto;
width:100%;
padding-left:20px;
`;

export const ChatArea = ({selectedProfile,Messages,appendMessage,setActivePage}) => {
 const classes = useStyles();
        const [newMsg,setNewMsg] = useState('')
        const [ProcessedMessages,updateMessages] = useState( Messages.filter((message) => {
        const checkSent = message.sender === selectedProfile.id;
        const checkRecvd = message.reciever === selectedProfile.id;
        return ( checkRecvd || checkSent)
    }))
  useEffect(() => {
     updateMessages(Messages.filter((message) => {
        const checkSent = message.sender === selectedProfile.id;
        const checkRecvd = message.reciever === selectedProfile.id;
        return ( checkRecvd || checkSent)
    })) 
  },[selectedProfile,Messages]);

   const handleEnterPressed = (event) => {
      if (event.key === 'Enter') {
        sendChat();
      }
   }
  const handleMsgChange = (parm) => {
    setNewMsg(parm.target.value)
  }
  const sendChat = () =>{
    if(newMsg.length === 0){
      return null
    }
      const pld = {
          message: newMsg,
          sender: 0,
          reciever: selectedProfile.id
      }
      
     appendMessage(pld);
     setNewMsg('') 
  } 

    return (
      <ThemeProvider theme={outerTheme}>
        <ChatAreaWrapper>
            <ChatHeader> 
              { screen.width<=700 &&(<IconButton onClick={() => setActivePage(1)}>
<ArrowBackIcon className={classes.icons}/>
</IconButton>)}
    <NameHolder onClick={() => setActivePage(3)} className={classes.text}>{selectedProfile.name}</NameHolder>

<IconHolder>
<StarIcon className={classes.icons}/>
</IconHolder>
<IconHolder>
<CallIcon className={classes.icons}/>
</IconHolder>
<IconHolder>
<VideocamIcon className={classes.icons}/>
</IconHolder>
               </ChatHeader>
            <ChatBody>
            {
                ProcessedMessages.map((message,index) =>(
                  message.sender === 0 ? (<Sent><SentMessageHolder>
                <ChatBGSent>{message.message}</ChatBGSent>
                <AvatarHolder><Avatar alt={selectedProfile.name} src='https://www.bosshunting.com.au/wp-content/uploads/2020/08/Snoop-Dogg.jpg'/></AvatarHolder>
                   </SentMessageHolder></Sent>) :(<Recieved><RecievedMessageHolder>
                     <AvatarHolder onClick={() => setActivePage(3)}><Avatar alt={selectedProfile.name} src={selectedProfile.profilePicture}/></AvatarHolder>
                     
                   <ChatBGRecieved> {message.message}</ChatBGRecieved>
                   </RecievedMessageHolder></Recieved>)
                ))
            }

            </ChatBody>
            <ChatInput>
                 <Paper className={classes.paperform} component="div">
      <IconButton>
        <AttachFileOutlinedIcon />
      </IconButton>
      <InputBase className={classes.input}
        placeholder="Type your Message"
        onKeyDown={handleEnterPressed}
        value={newMsg}
        onChange={handleMsgChange}
      />
      <IconButton onClick={() => sendChat()}>
        <SendIcon />
      </IconButton>
                 </Paper>
            </ChatInput>
        </ChatAreaWrapper>
        </ThemeProvider>
    )
}
