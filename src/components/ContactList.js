import React,{useState} from 'react'
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import styled from 'styled-components'
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';

const ContactsWrapper = styled.div`
display:grid;
grid-template-rows: 5rem 1fr; 
background:#232a32;
`;
const SearchHolder = styled.div`
border-bottom: 1px solid #ccc;
padding:10px 24px;
`;

const useStyles = makeStyles((theme) => ({
  paperform: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    borderRadius: '30px',
    background:'#363d45'
  },
    input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  divider:{
      width:40
  },
  list:{
      margin:12,
      cursor:'pointer'
  },
  avatarPic:{
      marginBottom:15
  },
  active:{
      borderLeft:'2px #3d62a6 solid',
        borderBottomLeftRadius: 2,
          borderTopLeftRadius: 2,
  }

}))
export const ContactList = ({Contacts,selectContact,selectedProfile,searchContacts}) => {
  const [searchTerm,setsearchTerm] = useState('')
     const classes = useStyles();
       const handleSearchTerm = (parm) => {
    setsearchTerm(parm.target.value)
  }
     const handleEnterPressed = (event) => {
      searchContacts(searchTerm);
   }
   const clearSearch = (() => {
     setsearchTerm('')
     searchContacts('')
   })
    return (
        <ContactsWrapper>
   
            <SearchHolder>

                <Paper className={classes.paperform} component="div">
                              <IconButton onClick={() => handleEnterPressed()}>
        <SearchIcon />
      </IconButton>
                          <InputBase className={classes.input}
        placeholder="Search"
                onKeyDown={handleEnterPressed}
        value={searchTerm}
        onChange={handleSearchTerm}
      />
         { searchTerm.length!=0 &&(<IconButton onClick={() => clearSearch()}>
        <CloseIcon />
      </IconButton>)}
                </Paper>
            </SearchHolder>
        <List>
            {Contacts.map((contact, index) =>(
                <>
            <ListItem className={`${classes.list} ${selectedProfile.id === contact.id ? classes.active : null}`} key={index} onClick={() => selectContact(contact.id)} alignItems="flex-start">
                <ListItemAvatar className={classes.avatar}>
                              <Avatar className={classes.avatarPic} alt={contact.name} src={contact.profilePicture} />
                               <Divider className={classes.divider}/>
                </ListItemAvatar>
                <ListItemText
                primary={contact.name}
                secondary = {contact.location}
                ></ListItemText>
                    
            </ListItem>
           
            </>))}
        
        </List>
        </ContactsWrapper>
    )
}
