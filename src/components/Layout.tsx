import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import AboutIcon from '@material-ui/icons/Help';
import MenuIcon from '@material-ui/icons/Menu';
import Head from 'next/head';
import Link from 'next/link';
import React,{useState,useEffect} from 'react';
import styled from 'styled-components';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import clsx from 'clsx';
import Divider from '@material-ui/core/Divider';
import { notify } from '../lib/notify';

import { SocialMedia } from '../seeder/SocialMedia';

const Wrapper = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: center;
  flex-direction: column;
`;
const Spacer = styled.div`
  height: 50px;
  width: 50px;
`;
const BodyWrapper = styled.div`
  display: flex;
`;
const MediaLogo = styled.img`
  height: 48px;
  width: 48px;
`;

const ContentWrapper = styled.div`
  flex-grow: 1;
  display: grid;
  grid-template-columns: 1fr 1.5fr 3fr 1.3fr;
  height: 100vh;
  @media only screen and (max-width: 768px) {
    grid-template-columns: unset;
  }
`;

const useStyles = makeStyles((theme) => ({
  drawer: {
    background: '#1c232b',
    flexShrink: 0,
    whiteSpace: 'nowrap',
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
      backgroundColor: '#1c232b'
    }
  },
  darkList: {
    background: '#1c232b',
    height: '100%'
  },
  hamburger: {
    width: '32px',
    height: '32px',
    margin: 'auto'
  },
  icons: {
    width: '64px',
    height: '64px',
    margin: 'auto'
  },
  active: {
    backgroundColor: '#262f34'
  }
}));

const Layout: any = ({ children,activePage }:any) => {
   const [isMobile, setIsMobile] = useState(true);

    useEffect(() => {
        if (screen.width >= 701) {
            setIsMobile(false);
        }
    }, []);

const classes = useStyles();
const theme = useTheme();

  return (
    <Wrapper>
      <Head>
        <title>MSM</title>
      </Head>
      <BodyWrapper>
 {!isMobile &&(<Drawer
 variant="permanent"
 className = {classes.drawer}
  classes={{ }}

 >
       <List className={classes.darkList}>
          <ListItem  button key='nav'>
              <ListItemIcon><Link href='#'><MenuIcon className={classes.hamburger} /></Link></ListItemIcon>
            </ListItem>
          {SocialMedia.map((media, index) => (
            <ListItem className={media.name === 'Messenger' ? classes.active : undefined} button key={media.name}>
              <ListItemIcon><Link href={media.link}><MediaLogo className={classes.icons} src={media.icon} /></Link></ListItemIcon>
            </ListItem>
          ))}
        </List>

 </Drawer>)}
 {isMobile ?(<ContentWrapper>
      {children[activePage]}
      </ContentWrapper>):
      (<ContentWrapper>
        {children}
      </ContentWrapper>)}
      </BodyWrapper>
    </Wrapper>
  );
};

export default Layout;
