import React from 'react'
import styled from 'styled-components'
import Paper from '@material-ui/core/Paper';
import NotificationsIcon from '@material-ui/icons/Notifications';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { createMuiTheme, ThemeProvider,makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
const outerTheme = createMuiTheme({
  palette: {
    type:'light'
  }

});

const useStyles = makeStyles((theme) => ({
    text:{
        color:'#000'
    },
    subtitle:{
        color:'#838488'
    }
}));

const ProfileWrapper= styled.div`
background:#fff;
display:grid;
grid-template-areas: 
    "header"
    "tile"
    "details";
grid-template-rows: 5rem auto 1fr;
`;

const ProfileHeader = styled.div`
grid-area: header;
display:grid;
grid-template-columns: 48px 1fr;
border-bottom: 1px solid #ccc;
@media only screen and (max-width: 768px) {
  grid-template-columns: 48px 48px 1fr;
}
`
const ProfileTile = styled.div`
grid-area: tile;
border-bottom: 1px solid #ccc;
`
const ProfileDetails = styled.div`
grid-area: details;
padding:1rem;

`
const NameHolder = styled.div`
margin:auto;

`;
const IconHolder = styled.div`
margin:auto;
`;
const PName = styled.span`
font-size: 1rem;
font-weight: 500;
`;
const AvatarHolder = styled.div`
height:100%;
`;
const ProfileDesc = styled.div`
margin:15px 0 28px 0;
text-align:center;
`;
const Grower = styled.div`
flex-grow:1;
`;

const SocialActions = styled.div`
display:flex;
padding:1rem;
`;
export const Profile = ({selectedProfile,setActivePage}) => {
     const classes = useStyles();
    return (
        <ThemeProvider theme={outerTheme}>
        <ProfileWrapper>
            <ProfileHeader>
                              { screen.width<=700 &&(<IconButton onClick={() => setActivePage(2)}>
<ArrowBackIcon className={classes.icons}/>
</IconButton>)}
                <IconHolder>
            <NotificationsIcon className={classes.subtitle}/>
            </IconHolder>
            <NameHolder>
    <PName className={classes.subtitle}>{selectedProfile.name}</PName>
    </NameHolder>
            </ProfileHeader>
            <ProfileTile>
                <SocialActions>
                   <IconHolder>
            <WhatsAppIcon className={classes.subtitle}/>
            </IconHolder>
<Grower/>
 <IconHolder>
            <MoreHorizIcon className={classes.subtitle}/>
            </IconHolder>
                </SocialActions>
                <AvatarHolder>
                <Avatar style={{margin:"auto",width:"180px",height:"180px",display:"block"}} alt={selectedProfile.name} src={selectedProfile.profilePicture}/>
                </AvatarHolder>
                <ProfileDesc>
    <Typography className={classes.text} variant="h5" gutterBottom>
        {selectedProfile.name}
        </Typography>
    <Typography className={classes.subtitle} variant="subtitle1">
        {selectedProfile.location}
    </Typography>
                </ProfileDesc>
            </ProfileTile>
            <ProfileDetails> 
                <List>     
               <ListItem>
                    <ListItemText className={classes.text} id='Nickname' primary='Nickname:' />
            <ListItemSecondaryAction>
                <Typography className={classes.subtitle} edge="end" variant="subtitle1">
        {selectedProfile.nickname}
    </Typography>
            </ListItemSecondaryAction>
                </ListItem>
                 <Divider />
                  <ListItem>
                    <ListItemText className={classes.text} id='Tel' primary='Tel:' />
            <ListItemSecondaryAction>
                <Typography className={classes.subtitle} edge="end" variant="subtitle1">
        {selectedProfile.tel}
    </Typography>
            </ListItemSecondaryAction>
                </ListItem>
                 <Divider />
                  <ListItem>
                    <ListItemText className={classes.text} id='dob' primary='Date Of Birth:' />
            <ListItemSecondaryAction>
                <Typography className={classes.subtitle} edge="end" variant="subtitle1">
        {`${selectedProfile.dob.getDate()}-${selectedProfile.dob.getMonth()}-${selectedProfile.dob.getYear()}`}
    </Typography>
            </ListItemSecondaryAction>
                </ListItem>
                 <Divider />
                  <ListItem>
                    <ListItemText className={classes.text} id='gender' primary='Gender:' />
            <ListItemSecondaryAction>
                <Typography className={classes.subtitle} edge="end" variant="subtitle1">
        {selectedProfile.gender}
    </Typography>
            </ListItemSecondaryAction>
                </ListItem>
                 <Divider />
                  <ListItem>
                    <ListItemText className={classes.text} id='lang' primary='Language:' />
            <ListItemSecondaryAction>
                <Typography className={classes.subtitle} edge="end" variant="subtitle1">
        {selectedProfile.language}
    </Typography>
            </ListItemSecondaryAction>
                </ListItem>

                </List>


                 </ProfileDetails>
        </ProfileWrapper>
        </ThemeProvider>
    )
}
