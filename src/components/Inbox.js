import React from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import Divider from '@material-ui/core/Divider';
import styled from 'styled-components'
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
const InboxWrapper = styled.div`
display: grid;
grid-template-rows: 5rem 1fr;
background:#1c232b;
`;

const ListWrapper = styled.div`
min-width:240px;
padding: 0 48px;
`;

const DividerWrapper = styled.div`
margin:16px;
`

const InboxHeader = styled.div`
font-size:22px;
font-weight:700;
margin:auto;
`
const Title = styled.div`
display: inline;
padding-right:3rem;`;
export const Inbox = () => {

  const handleListItemClick = ((e,indx) => {
    console.log('Feature will be implemented')
  })
    const selectedIndex = 1;
    return (
      <InboxWrapper>
        
        <InboxHeader>
          <div>
          <Title>
          Inbox
</Title>
              <IconButton size="medium" style={{border:"solid #ccc"}} aria-label="delete">
        <GroupAddIcon />
      </IconButton>
      </div>
        </InboxHeader>
        <ListWrapper>
        <List>
 <ListItem
          button
          selected={selectedIndex === 0}
          onClick={(event) => handleListItemClick(event, 0)}
        >
             <ListItemText primary="All Messages" />
             <ListItemSecondaryAction>
   <Typography edge="end" component="div">
      <Box fontWeight="fontWeightLight" m={1}>
        3
      </Box>
      </Typography>
             </ListItemSecondaryAction>
        </ListItem>
         <ListItem
          button
          selected={selectedIndex === 1}
          onClick={(event) => handleListItemClick(event, 0)}
        >
             <ListItemText primary="Unread" />
             <ListItemSecondaryAction>
   <Typography edge="end" component="div">
      <Box fontWeight="fontWeightLight" m={1}>
        3
      </Box>
      </Typography>
             </ListItemSecondaryAction>
        </ListItem>
        <DividerWrapper>
        <Divider />
        </DividerWrapper>
         <ListItem
          button
          selected={selectedIndex === 0}
          onClick={(event) => handleListItemClick(event, 0)}
        >
             <ListItemText primary="Help" />

        </ListItem>
         <ListItem
          button
          selected={selectedIndex === 0}
          onClick={(event) => handleListItemClick(event, 0)}
        >
             <ListItemText primary="Settings" />

        </ListItem>
        </List>
        </ListWrapper></InboxWrapper>
    )
}
