// {name,location,dob,nickname,tel,id,gender,language,id,profile pic}
// id 0 = user

export const Contacts = [
{
    name: "Bruce Wayne",
    location: "Gotham",
        dob: new Date(1939, 11, 24, 10, 33),
        nickname:"Bat",
        tel:"4444444444",
        gender:'Male',
        language:'English',
        id:1,
        profilePicture:'https://image.flaticon.com/icons/svg/1674/1674291.svg'
},

    {
        name: "Wonder Woman",
        location: "Amazon",
        dob: new Date(1950, 1, 4, 1, 33),
        nickname: "Gal",
        tel: "3336662223",
        gender: 'Female',
        language: 'English',
        id: 2,
        profilePicture: 'https://image.flaticon.com/icons/svg/773/773253.svg'
    },

    {
        name: "All Might",
        location: "Japan",
        dob: new Date(2016, 11, 24, 10, 33),
        nickname: "Toshinori",
        tel: "1111111111",
        gender: 'Male',
        language: 'Japanese',
        id: 3,
        profilePicture: 'https://gitlab.com/uploads/-/system/user/avatar/5990984/avatar.png?width=400'
    },

    {
        name: "Goku",
        location: "East District",
        dob: new Date(1984, 12, 3, 10, 33),
        nickname: "Kakarot",
        tel: "9000000001",
        gender: 'Male',
        language: 'English',
        id: 4,
        profilePicture: 'https://avatarfiles.alphacoders.com/213/213425.jpg'
    },

    {
        name: "Haruhi Suzumiya",
        location: "Japan",
        dob: new Date(2004, 12, 26, 2, 33),
        nickname: "SNS",
        tel: "9999999999",
        gender: 'Female',
        language: 'Japanese',
        id: 5,
        profilePicture: 'https://avatarfiles.alphacoders.com/153/153672.jpg'
    },

]