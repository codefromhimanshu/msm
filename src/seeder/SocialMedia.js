export const SocialMedia = [
    {
        name:'Messenger',
        icon:'https://upload.wikimedia.org/wikipedia/commons/3/3b/Facebook_Messenger_logo.svg',
        link:'/'
    },
    {
        name:'Whatsapp',
        icon:'https://upload.wikimedia.org/wikipedia/commons/6/6b/WhatsApp.svg',
        link:'/chat/whatsapp'
    }
]