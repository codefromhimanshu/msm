import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../index';

export const chatSelector = (state: RootState, _props?: any) => state.chat;

export const propsIdSelector = (state: RootState, props: { id: number } & any) => props.id;

export const chatByIdSelector = createSelector(
  chatSelector,
  propsIdSelector,
  (chats, id) => chats.find((chat) => chat.id === id)
);
