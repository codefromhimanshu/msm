// The Default state for the db
import { State } from './types';
import {Messages} from '../../seeder/Messages'

export const defaultState: State = Messages;
export default defaultState;
