import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '../../store';
import initialState from './state';
import { Chat } from './types';
import { doSomethingAsync } from './utils';

export const slice = createSlice({
  name: 'chat',
  initialState,
  reducers: {
    addChat(state, action: PayloadAction<Chat>) {
      state.push(action.payload);
    },
  },
});

export const { addChat } = slice.actions;

export default slice.reducer;

export const addChatThunk = (chat: Chat): AppThunk => async (dispatch) => {
  try {
    const mutatedChat = await doSomethingAsync(chat);
    dispatch(addChat(mutatedChat));
  } catch (err) {
    // Log an error here
  }
};
